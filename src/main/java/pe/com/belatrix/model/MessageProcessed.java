/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.belatrix.model;

import java.util.logging.Level;

/**
 *
 * @author petervs
 */
public class MessageProcessed {

    private String messageFull;
    private Integer type;
    private Level levelLog;

    public String getMessageFull() {
        return messageFull;
    }

    public void setMessageFull(String messageFull) {
        this.messageFull = messageFull;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Level getLevelLog() {
        return levelLog;
    }

    public void setLevelLog(Level levelLog) {
        this.levelLog = levelLog;
    }
    
}
