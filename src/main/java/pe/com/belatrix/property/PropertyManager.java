/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.belatrix.property;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author petervs
 */
public class PropertyManager {

    private static final Logger logger = Logger.getLogger(PropertyManager.class);

    public static String userName;
    public static String password;
    public static String dbms;
    public static String serverName;
    public static String portNumber;
    public static String logFileFolder;

    static {
        try {
            Properties prop = new Properties();
            prop.load(PropertyManager.class.getResourceAsStream("/app.properties"));

            userName = prop.getProperty("userName");
            password = prop.getProperty("password");
            dbms = prop.getProperty("dbms");
            serverName = prop.getProperty("serverName");
            portNumber = prop.getProperty("portNumber");
            logFileFolder = prop.getProperty("logFileFolder");
        } catch (FileNotFoundException ex) {
            logger.error("Error Inicializando PropertyManager [...]");
            logger.error(ex.getMessage(), ex);
        } catch (IOException ex) {
            logger.error("Error Inicializando PropertyManager [...]");
            logger.error(ex.getMessage(), ex);
        }
    }

}
