/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.belatrix.logmessageapp;

import java.text.DateFormat;
import java.util.Date;
import java.util.logging.Level;
import pe.com.belatrix.implement.LogMessageConsole;
import pe.com.belatrix.implement.LogMessageDataBase;
import pe.com.belatrix.implement.LogMessageFile;
import pe.com.belatrix.interfaz.LogMessage;
import pe.com.belatrix.model.Message;
import pe.com.belatrix.model.MessageProcessed;

/**
 *
 * @author petervs
 */
public class JobLoggerRefactoring {

    private final boolean logToFile;
    private final boolean logToConsole;
    private final boolean logMessage;
    private final boolean logWarning;
    private final boolean logError;
    private final boolean logToDatabase;

    public JobLoggerRefactoring(boolean logToFileParam, boolean logToConsoleParam, boolean logToDatabaseParam,
            boolean logMessageParam, boolean logWarningParam, boolean logErrorParam) {
        logError = logErrorParam;
        logMessage = logMessageParam;
        logWarning = logWarningParam;
        logToDatabase = logToDatabaseParam;
        logToFile = logToFileParam;
        logToConsole = logToConsoleParam;
    }

    public void LogMessage(Message message) throws Exception {
        validationMessage(message);
        MessageProcessed messageProcessed = getMessageProcessed(message);
        if (null != messageProcessed.getType()) {
            if (logToFile) {
                LogMessage logMessageFile = new LogMessageFile();
                logMessageFile.registerMessage(messageProcessed.getMessageFull(), messageProcessed.getType(), messageProcessed.getLevelLog());
            } else if (logToConsole) {
                LogMessage logMessageConsole = new LogMessageConsole();
                logMessageConsole.registerMessage(messageProcessed.getMessageFull(), messageProcessed.getType(), messageProcessed.getLevelLog());
            } else if (logToDatabase) {
                LogMessage logMessageDataBase = new LogMessageDataBase();
                logMessageDataBase.registerMessage(messageProcessed.getMessageFull(), messageProcessed.getType(), messageProcessed.getLevelLog());
            }
        }
    }

    private MessageProcessed getMessageProcessed(Message message) {
        MessageProcessed messageProcessed = new MessageProcessed();
        if (message.isError() && logError) {
            messageProcessed.setType(1);
            messageProcessed.setLevelLog(Level.SEVERE);
            messageProcessed.setMessageFull("error " + DateFormat.getDateInstance(DateFormat.LONG).format(new Date()) + " " + message.getMessageText().trim());
        }

        if (message.isWarning() && logWarning) {
            messageProcessed.setType(2);
            messageProcessed.setLevelLog(Level.WARNING);
            messageProcessed.setMessageFull("warning " + DateFormat.getDateInstance(DateFormat.LONG).format(new Date()) + " " + message.getMessageText().trim());
        }

        if (message.isMessage() && logMessage) {
            messageProcessed.setType(3);
            messageProcessed.setLevelLog(Level.INFO);
            messageProcessed.setMessageFull("message " + DateFormat.getDateInstance(DateFormat.LONG).format(new Date()) + " " + message.getMessageText().trim());
        }
        return messageProcessed;
    }

    public void validationMessage(Message message) throws Exception {
        if (invalidMessage(message.getMessageText())) {
            throw new Exception("Message Empty");
        }
        if (invalidConfiguration()) {
            throw new Exception("Invalid configuration Log must be specified");
        }
        if (invalidSpecification(message.isMessage(), message.isWarning(), message.isError())) {
            throw new Exception("Error or Warning or Message must be specified");
        }
    }

    public boolean invalidMessage(String messageText) {
        return messageText == null || messageText.trim().length() == 0;
    }

    public boolean invalidConfiguration() {
        return !logToConsole && !logToFile && !logToDatabase;
    }

    public boolean invalidSpecification(boolean message, boolean warning, boolean error) {
        return (!logError && !logMessage && !logWarning) && (!message && !warning && !error);
    }

}
