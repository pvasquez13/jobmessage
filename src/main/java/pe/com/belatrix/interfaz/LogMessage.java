/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.belatrix.interfaz;

import java.util.logging.Level;

/**
 *
 * @author petervs
 */
public interface LogMessage {

    void registerMessage(String messageFull, Integer type, Level level);
}
