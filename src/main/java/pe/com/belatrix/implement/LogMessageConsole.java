/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.belatrix.implement;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.com.belatrix.interfaz.LogMessage;

/**
 *
 * @author petervs
 */
public class LogMessageConsole implements LogMessage {

    private static final Logger LOG = Logger.getLogger("MyLog");
    static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(LogMessageConsole.class);

    @Override
    public void registerMessage(String messageFull, Integer type, Level level) {
        logger.info("Save Message in Console");
        LOG.addHandler(new ConsoleHandler());
        LOG.log(level, messageFull);
    }

}
