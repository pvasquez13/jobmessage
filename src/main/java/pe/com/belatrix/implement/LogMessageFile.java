/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.belatrix.implement;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.com.belatrix.interfaz.LogMessage;
import pe.com.belatrix.property.PropertyManager;

/**
 *
 * @author petervs
 */
public class LogMessageFile implements LogMessage {
    
    private static final Logger LOG = Logger.getLogger("MyLog");
    static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(LogMessageDataBase.class);

    @Override
    public void registerMessage(String messageFull, Integer type, Level level) {
        try {
            logger.info("Save Message in File");
            FileHandler fh = new FileHandler(PropertyManager.logFileFolder + "\\logFile.txt");
            LOG.addHandler(fh);
            LOG.log(level, messageFull);
        } catch (IOException | SecurityException ex) {
            Logger.getLogger(LogMessageFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
