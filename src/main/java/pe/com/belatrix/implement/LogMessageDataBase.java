/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.belatrix.implement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import pe.com.belatrix.database.FactoryDBConnection;
import pe.com.belatrix.interfaz.LogMessage;

/**
 *
 * @author petervs
 */
public class LogMessageDataBase implements LogMessage {

    private final FactoryDBConnection factory;
    static Logger logger = Logger.getLogger(LogMessageDataBase.class);

    public LogMessageDataBase() {
        factory = FactoryDBConnection.getInstance();
    }

    @Override
    public void registerMessage(String messageFull, Integer type, Level level) {

        try {
            logger.info("Save Message in DataBase");
            Connection connection = factory.getDataBaseConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("insert into Log_Values(?,?)");
            preparedStatement.setString(1, messageFull);
            preparedStatement.setInt(2, type);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            java.util.logging.Logger.getLogger(LogMessageDataBase.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
