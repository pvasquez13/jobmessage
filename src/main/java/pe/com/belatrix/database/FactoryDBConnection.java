/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.belatrix.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import org.apache.log4j.Logger;
import pe.com.belatrix.property.PropertyManager;

/**
 *
 * @author petervs
 */
public class FactoryDBConnection {

    private static final Logger LOG = Logger.getLogger(FactoryDBConnection.class);
    private final Connection connection;
    private static FactoryDBConnection factoryDBConnection;

    public FactoryDBConnection() {
        this.connection = this.getDataBaseConnection();
    }

    public static FactoryDBConnection getInstance() {
        if (factoryDBConnection == null) {
            factoryDBConnection = new FactoryDBConnection();
        }
        return factoryDBConnection;
    }

    public Connection getDataBaseConnection() {

        try {
            Properties connectionProps = new Properties();
            connectionProps.put("user", PropertyManager.userName);
            connectionProps.put("password", PropertyManager.password);
            Connection connectionDB = DriverManager.getConnection("jdbc:" + PropertyManager.dbms + "://" + PropertyManager.serverName
                    + ":" + PropertyManager.portNumber + "/", connectionProps);

            return connectionDB;
        } catch (SQLException ex) {
            LOG.error("Error connection");
            LOG.error(ex.getMessage(), ex);
        }
        return null;

    }

    public Connection getConnection() {
        return connection;
    }

}
