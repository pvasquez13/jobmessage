/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.belatrix.logmessageapp;

import org.junit.Test;
import pe.com.belatrix.model.Message;

/**
 *
 * @author petervs
 */
public class JobLoggerRefactoringTest {

    /**
     * Test of LogMessage method, of class JobLoggerRefactoring.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testLogMessageOnlyErrorConsole() throws Exception {
        System.out.println("testLogMessageOnlyErrorConsole");
        boolean message = false;
        boolean warning = false;
        boolean error = true;
        boolean logToFile = false;
        boolean logToConsole = true;
        boolean logToDatabase = false;
        Message messageObject = new Message();
        messageObject.setError(true);
        messageObject.setMessageText("THIS IS A MESSAGE FOR THE APPLICATION");
        JobLoggerRefactoring jobLoggerRefactoring = new JobLoggerRefactoring(logToFile, logToConsole, logToDatabase, message, warning, error);
        jobLoggerRefactoring.LogMessage(messageObject);
    }

    @Test
    public void testLogMessageOnlyErrorAndWarningInFile() throws Exception {
        System.out.println("testLogMessageOnlyErrorAndWarningInFile");
        boolean message = false;
        boolean warning = true;
        boolean error = true;
        boolean logToFile = true;
        boolean logToConsole = false;
        boolean logToDatabase = false;
        Message messageObject = new Message();
        messageObject.setWarning(true);
        messageObject.setMessageText("THIS IS A MESSAGE FOR THE APPLICATION");
        JobLoggerRefactoring jobLoggerRefactoring = new JobLoggerRefactoring(logToFile, logToConsole, logToDatabase, message, warning, error);
        jobLoggerRefactoring.LogMessage(messageObject);
    }

}
